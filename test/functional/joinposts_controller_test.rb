require 'test_helper'

class JoinpostsControllerTest < ActionController::TestCase
  setup do
    @joinpost = joinposts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:joinposts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create joinpost" do
    assert_difference('Joinpost.count') do
      post :create, joinpost: { name: @joinpost.name, post: @joinpost.post }
    end

    assert_redirected_to joinpost_path(assigns(:joinpost))
  end

  test "should show joinpost" do
    get :show, id: @joinpost
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @joinpost
    assert_response :success
  end

  test "should update joinpost" do
    put :update, id: @joinpost, joinpost: { name: @joinpost.name, post: @joinpost.post }
    assert_redirected_to joinpost_path(assigns(:joinpost))
  end

  test "should destroy joinpost" do
    assert_difference('Joinpost.count', -1) do
      delete :destroy, id: @joinpost
    end

    assert_redirected_to joinposts_path
  end
end
