# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.new(email: 'choo@gmail.com', password: 'choo1234', password_confirmation: 'choo1234', name: 'choo1234')
user.admin = true
user.confirmed_at = Time.now
user.save