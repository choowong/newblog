class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.text :email
      t.boolean :status
      t.string :detailadmin
      t.string :post

      t.timestamps
    end
  end
end
