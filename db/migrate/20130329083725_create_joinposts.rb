class CreateJoinposts < ActiveRecord::Migration
  def change
    create_table :joinposts do |t|
      t.string :post
      t.text :name

      t.timestamps
    end
  end
end
