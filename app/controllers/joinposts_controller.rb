class JoinpostsController < ApplicationController
  # GET /joinposts
  # GET /joinposts.json
  def index
    @joinposts = Joinpost.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @joinposts }
    end
  end

  # GET /joinposts/1
  # GET /joinposts/1.json
  def show
    @joinpost = Joinpost.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @joinpost }
    end
  end

  # GET /joinposts/new
  # GET /joinposts/new.json
  def new
    @joinpost = Joinpost.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @joinpost }
    end
  end

  # GET /joinposts/1/edit
  def edit
    @joinpost = Joinpost.find(params[:id])
  end

  # POST /joinposts
  # POST /joinposts.json
  def create
    @joinpost = Joinpost.new(params[:joinpost])

    respond_to do |format|
      if @joinpost.save
        format.html { redirect_to @joinpost, notice: 'Joinpost was successfully created.' }
        format.json { render json: @joinpost, status: :created, location: @joinpost }
      else
        format.html { render action: "new" }
        format.json { render json: @joinpost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /joinposts/1
  # PUT /joinposts/1.json
  def update
    @joinpost = Joinpost.find(params[:id])

    respond_to do |format|
      if @joinpost.update_attributes(params[:joinpost])
        format.html { redirect_to @joinpost, notice: 'Joinpost was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @joinpost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /joinposts/1
  # DELETE /joinposts/1.json
  def destroy
    @joinpost = Joinpost.find(params[:id])
    @joinpost.destroy

    respond_to do |format|
      format.html { redirect_to joinposts_url }
      format.json { head :no_content }
    end
  end
end
