class CommentsController < ApplicationController

  def create
    @post = Post.find(params[:post_id])

    @comment = @post.comments.create(params[:comment].merge({commenter: current_user.name}))



    respond_to do |format|
        format.html { redirect_to post_path(@post) }
        format.js 
        end
	end

  def destroy
  @comment = Comment.find(params[:id])
  @commentable = @comment.commentable
  if @comment.destroy
    redirect_to @commentable
  end
end


end