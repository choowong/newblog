class PostsController < ApplicationController
  before_filter :authenticate_user!, only: [:update, :create, :destroy, :new, :edit]
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
    @post1 = Post.last
    @post2 = Post.order("created_at DESC").limit(1).offset(1).last
    @post3 = Post.order("created_at DESC").limit(1).offset(2).last
    @post4 = Post.order("created_at DESC").limit(1).offset(3).last
    @post5 = Post.order("created_at DESC").limit(1).offset(4).last
    @post6 = Post.order("created_at DESC").limit(1).offset(5).last

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new
    if current_user.try(:admin?)
        respond_to do |format|

            format.html # new.html.erb
            format.json { render json: @post }
        end
    else 
      flash[:error] = "You don't have permission to create new post"
      redirect_to posts_path
    end
  end

  # GET /posts/1/edit
  def edit
      if current_user.try(:admin?)
        @post = Post.find(params[:id])
      else
        redirect_to posts_path
       end 
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post])

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
      end
    end
  end
